const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const config = {
        entry: {
            'app': './src/app.module.js'
        },
        output: {
            filename: 'libs/[name].bundle.js',
            path: path.resolve(__dirname, 'build')
        },

        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: ['ng-annotate-loader', 'babel-loader'],
                },
                { test: /\.html$/, loader: 'html-loader' },
                {
                    test: /\.(scss)$/,
                    use: ExtractTextWebpackPlugin.extract({
                        use: [
                            {
                                loader: "css-loader",
                                options: {
                                    minimize: true
                                }
                            },
                            {
                                loader: "sass-loader"
                            }
                        ]
                    })
                },
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html'
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor',
                filename: 'libs/[name].bundle.js'
            }),
            new CleanWebpackPlugin('build'),
            new ExtractTextWebpackPlugin('styles/style.css')
        ],
        devServer: {
            port: 3001,
            contentBase: './src/',
            historyApiFallback: true
        }
    };


module.exports = config;