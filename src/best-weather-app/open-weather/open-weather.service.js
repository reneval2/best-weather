class OpenWeatherService {
    /* @ngInject */
    constructor($http, API, APP_PARAM) {
        this.API = API;
        this.APP_PARAM = APP_PARAM;
        this.$http = $http;
    }

    loadData(bbox) {
        const request = {
            APPID: this.API.APPID,
            bbox: [bbox.left, bbox.bottom, bbox.right, bbox.top, bbox.zoom].join(',')
        };

        return this.$http({
            method: 'GET',
            url: this.API.URL,
            params: request
        }).then(function (resp) {
            return resp.data.list;
        })
            .catch(function (error) {
                return error;
            });
    }

    calcData(data = [], shift = 0) {
        return data.map(i => {
            const {name, main: {temp, humidity}} = i;
            let delta = (
                Math.abs(temp - this.APP_PARAM.temp + Number.parseInt(shift)) + Math.abs(humidity - this.APP_PARAM.humidity)
            ).toFixed(2);
            return {name, temp, humidity, delta};
        })
            .sort((a, b) => a.delta - b.delta)
    }
}

export default OpenWeatherService;