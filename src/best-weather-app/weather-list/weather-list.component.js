import template from "./weather-list.component.html"


class WeatherListController {

    /* @ngInject */
    constructor() {
    }
}

const WeatherListComponent = {
    controller: WeatherListController,
    template,
    bindings: {
        stationList: '<'
    }
};

export default WeatherListComponent;