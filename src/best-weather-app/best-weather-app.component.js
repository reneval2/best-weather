// for loading styles we need to load main scss file

import template from "./best-weather-app.component.html"

class BestWeatherAppController {
    /* @ngInject */
    constructor(OpenWeatherService) {
        this.OpenWeatherService = OpenWeatherService;
        this.resultList = [];
    }

    onSelectParams(params ,sex) {

        this.resultList = [];
            this.OpenWeatherService
            .loadData(params)
            .then(res => {
                this.resultList = this.OpenWeatherService
                    .calcData(res,sex);
            })
    }
}

const BestWeatherAppComponent = {
    controller: BestWeatherAppController,
    template
};

export default BestWeatherAppComponent;