import angular from "angular"
import template from "./select-params.component.html"


class SelectParamsController {

    /* @ngInject */
    constructor() {
        this.sex = "0";
        this.rect = {
            bottom: 31,
            left: 34,
            top: 33,
            right: 36,
            zoom: 10
        }
    }

    onSubmit() {
        this.onSelectParams({rect: this.rect, sex: this.sex});
    }
}

const component = {
    controller: SelectParamsController,
    template,
    bindings: {
        onSelectParams: '&'
    }
};

export default component;