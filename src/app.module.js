import styles from './styles/style.scss';

import angular from "angular"
import BestWeatherAppComponent from "./best-weather-app/best-weather-app.component"
import SelectParamsComponent from "./best-weather-app/select-papams/select-params.component"
import WeatherListComponent from "./best-weather-app/weather-list/weather-list.component"
import * as ngMap from "ngmap/build/scripts/ng-map.min.js";
import OpenWeatherService from "./best-weather-app/open-weather/open-weather.service";

import {API, APP_PARAM} from "./app.constants";


angular.module("app", ['rx', 'ngMap']);
export default angular
    .module("app", [])
    .component("bestWeatherApp", BestWeatherAppComponent)
    .component("weatherList", WeatherListComponent)
    .component("selectParams", SelectParamsComponent)
    .service("OpenWeatherService", OpenWeatherService)
    .constant("API", API)
    .constant("APP_PARAM", APP_PARAM)
    .name